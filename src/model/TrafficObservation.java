package model;

import java.util.ArrayList;

public class TrafficObservation {

    String name;
    String date;
    int speedLimit;
    ArrayList<VehicleObservation> vehicleObservationArrayList;

    public TrafficObservation(String name, String date, int speedLimit) {
        this.name = name;
        this.date = date;
        this.speedLimit = speedLimit;
        this.vehicleObservationArrayList = new ArrayList<VehicleObservation>();
    }

    public void addVehicleObservation(VehicleObservation observation){
        vehicleObservationArrayList.add(observation);
    }

    public ArrayList<VehicleObservation> getTrafficViolators(){
        ArrayList<VehicleObservation> violators = new ArrayList<VehicleObservation>();
        for (VehicleObservation vehicleObservation : vehicleObservationArrayList){
            if (vehicleObservation.speed > speedLimit){
                violators.add(vehicleObservation);
            }
        }
        return violators;
    }

    public String toString(){
        return name + " " + date;
    }
}
