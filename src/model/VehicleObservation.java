package model;

/**
 * Created by Andy on 13/01/14.
 */
public class VehicleObservation {
    String licensePlate;
    int speed;
    int hour;
    int minute;

    public VehicleObservation(String licensePlate, int speed, int hour, int minute) {
        this.licensePlate = licensePlate;
        this.speed = speed;
        this.hour = hour;
        this.minute = minute;
    }

    @Override
    public String toString() {
        return licensePlate;
    }

    public String getAllData(){
        return "VehicleObservation{" +
                "licensePlate='" + licensePlate + '\'' +
                ", speed=" + speed +
                ", hour=" + hour +
                ", minute=" + minute +
                '}';
    }
}