package model;

/**
 * User: lasse Date: 1/13/14 Time: 9:46 AM
 */

public class TrafficTicket {

    TrafficObservation traffic;
    VehicleObservation vehicle;
    int ticketPrice;

    public TrafficTicket(TrafficObservation traffic, VehicleObservation vehicle){
        this.traffic = traffic;
        this.vehicle = vehicle;
        this.ticketPrice = 0;
    }

    private void calculateFine(){
        float limit = traffic.speedLimit;
        float speed = vehicle.speed;
        int ticket = 0;

        if(speed > limit){
            float percent = ((speed - limit) / limit) * 100;
            if(0 < percent && percent < 20){
                ticket = 1000;
            } else if(20 <= percent && percent < 30){
                ticket = 1500;
            } else if(30 <= percent && percent < 40){
                ticket = 2500;
            } else if(40 <= percent && percent < 50){
                ticket = 3500;
            } else if(50 <= percent){
                ticket = 4500;
            }
            this.ticketPrice = ticket;
        }
    }

    public String toString(){
        calculateFine();
        String response;
        response = "Fartbøde for køretøj, " + vehicle.licensePlate+"\n";
        response += "Tidspunkt: "+traffic.date+" kl. "+vehicle.hour+":"+vehicle.minute+"\n";
        response += "Strækning: "+traffic.name+", Hastihedsbegrænsning: "+traffic.speedLimit+" km/t\n";
        response += "Målt hastighed: "+vehicle.speed+" km/t\n";
        response += "Bøde: "+ticketPrice+" kr.";

        return response;
     }
}
