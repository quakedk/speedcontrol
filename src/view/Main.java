package view;

import model.TrafficObservation;
import model.TrafficTicket;
import model.VehicleObservation;

import javax.swing.*;

/**
 * Created by Andy on 13/01/14.
 */
public class Main {

    public static void main(String[] args) {
        //Create JFrame
        JFrame frame = new JFrame("Epic SpeedCatcher 2014 - Pro Edition");
        frame.setContentPane(new MainWindow().panelForm);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.setVisible(true);

        TrafficObservation to = new TrafficObservation("Edith Rodes Vej", "13. Januar, 2014", 100);
        VehicleObservation vo = new VehicleObservation("AB1452051", 130, 12, 15);
        TrafficTicket tt = new TrafficTicket(to, vo);

        System.out.println(tt);

    }

}
