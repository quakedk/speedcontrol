package view;

import model.TrafficObservation;
import model.TrafficTicket;
import model.VehicleObservation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Andy on 13/01/14.
 */
public class MainWindow {
    public JPanel panelForm;
    private JButton btnCreateTrafficObservation;
    private JTextField txtDate;
    private JTextField txtSpeedLimit;
    private JTextField txtName;
    private JTextField txtLicense;
    private JTextField txtSpeed;
    private JTextField txtHour;
    private JTextField txtMinute;
    private JButton btnAddObservation;
    private JComboBox comboTrafficObs2;
    private JComboBox comboRoadDevil;
    private JTextArea txtArea;
    private JComboBox comboTrafficObs1;
    private JLabel lblTraffcObs1;

    //combo models
    DefaultComboBoxModel listTrafficObservations1;
    DefaultComboBoxModel listTrafficObservations2;
    DefaultComboBoxModel listSpeedDevils;

    public MainWindow() {

        btnCreateTrafficObservation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TrafficObservation observation = new TrafficObservation(txtName.getText(),txtDate.getText(),Integer.parseInt(txtSpeedLimit.getText()));
                listTrafficObservations1.addElement(observation);
                listTrafficObservations2.addElement(observation);

            }
        });


        btnAddObservation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VehicleObservation measurement = new VehicleObservation(
                        txtLicense.getText(),
                        Integer.parseInt(txtSpeed.getText()),
                        Integer.parseInt(txtHour.getText()),
                        Integer.parseInt(txtMinute.getText())
                );

                TrafficObservation place = (TrafficObservation) comboTrafficObs1.getSelectedItem();
                place.addVehicleObservation(measurement);

                refreshSpeedDevilCombo();
            }
        });
        comboTrafficObs2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshSpeedDevilCombo();
            }
        });
        comboRoadDevil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(comboRoadDevil.getSelectedItem() instanceof VehicleObservation){
                    VehicleObservation roadDevil = (VehicleObservation) comboRoadDevil.getSelectedItem();
                    TrafficObservation to = (TrafficObservation) comboTrafficObs2.getSelectedItem();

                    TrafficTicket tt = new TrafficTicket(to, roadDevil);
                    txtArea.setText(tt.toString());
                }
            }
        });
    }

    public void refreshSpeedDevilCombo(){
        TrafficObservation place = (TrafficObservation) comboTrafficObs2.getSelectedItem();

        ArrayList<VehicleObservation> trafficViolators = place.getTrafficViolators();

        listSpeedDevils.removeAllElements();

        for (VehicleObservation speedDevil : trafficViolators){
            listSpeedDevils.addElement(speedDevil);
        }
    }

    private void createUIComponents() {

        listTrafficObservations1 = new DefaultComboBoxModel();
        listTrafficObservations2 = new DefaultComboBoxModel();
        comboTrafficObs1 = new JComboBox(listTrafficObservations1);
        comboTrafficObs2 = new JComboBox(listTrafficObservations2);

        listSpeedDevils = new DefaultComboBoxModel();
        comboRoadDevil = new JComboBox(listSpeedDevils);

    }
}